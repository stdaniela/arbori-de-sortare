//trebuie sa ma hotarasc ce metode pastrez (de care am nevoie)
package arboridesortare;

public class Nod {
    private int valoare; // informatia din nod
    private Nod st; // fiul din stanga
    private Nod dr; // fiul din dreapta
    
    public Nod(int valoare) {
        this.valoare = valoare;
        this.st = null;
        this.dr = null;
    }
    
    public void setValoare(int valoare) {
        this.valoare = valoare;
    }
    
    public int getValoare() {
        return valoare;
    }
    
    public void setNodSt(Nod st) {
        this.st = st;
    }
    
    public Nod getNodSt() {
        return st;
    }
    
    public void setNodDr(Nod dr) {
        this.dr = dr;
    }
    
    public Nod getNodDr() {
        return dr;
    }
}
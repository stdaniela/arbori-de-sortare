package arboridesortare;

import javax.swing.JOptionPane;

public class Arbore {
    private Nod radacina;
    private int x, y;
    private int prevX, prevY;
    private String sirValori;
    private int valoare;
    
    public Arbore(int x, int y) {
        radacina = null;
        prevX = 0;
        prevY = 0;
        this.x = x;
        this.y = y;
        sirValori = "";
    }
    
    public Nod getRadacina() {
        return radacina;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public int getPrevX() {
        return prevX;
    }
    
    public int getPrevY() {
        return prevY;
    }
    
    public int getValoare() {
        return valoare;
    }
    
    public Nod cautaNod(int valoare) {
        Nod curent = radacina;
        if (curent == null) {
            return null; // nu exista nici un nod in arbore
        }
        while (curent.getValoare() != valoare) {
            if (valoare < curent.getValoare()) {
                curent = curent.getNodSt();
            } else {
                curent = curent.getNodDr();
            }
            if (curent == null) {//nu exista descendenti
                return null;
            }
        }
        return curent;
    }
    
    public void insereazaNod(int valoare) {
        Nod nod = new Nod(valoare);
        int k = 0;
        if (radacina == null) {
            this.valoare = valoare;
            radacina = nod;
            prevX = 0;
            prevY = 0;
        } else {
            Nod curent = radacina;
            
            while (true) {
                k++;
                if (k <= 4) { //arborele desenat permite 5 nivele (0-4)
                    prevX = x;
                    prevY = y;
                    y+=50;
                    if (valoare < curent.getValoare()) {
                        x -= (int)( (50 * Math.pow(2, 4-k))/2 );
                        if(curent.getNodSt() == null) {
                            curent.setNodSt(nod);
                            this.valoare = valoare;
                            return;
                        } else {
                            curent = curent.getNodSt();
                        }
                    } else {
                        prevX = x;
                        x += (int)( (50 * Math.pow(2, 4-k))/2 );
                        if(curent.getNodDr() == null) {
                            curent.setNodDr(nod);
                            this.valoare = valoare;
                            return;
                        } else {
                            curent = curent.getNodDr();
                        }
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(NewFrame.getFrame(),
                            "Valoarea introdusa nu poate fi reprezentata grafic (depaseste limitele ferestrei)"
                                    + "\nAceasta valoare nu a fost introdusa in arbore",
                            "Depasire", JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
        }
    }
    
    public String getSirValori() {
        return sirValori;
    }
    
    public void inordine(Nod m) {
        if (m != null) {
            inordine(m.getNodSt());
            sirValori += m.getValoare() + ", ";
            //System.out.println(m.getValoare() + " ");
            inordine(m.getNodDr());
        }
    }
}

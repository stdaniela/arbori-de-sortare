package arboridesortare;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NewFrame{
    private Arbore a;
    private static JFrame frame;
    private int x,y, prevX, prevY;
    private String val;
    
    public NewFrame() {
        frame = new JFrame("Arbori de sortare");
        frame.setSize(800,400);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        a = new Arbore((frame.getWidth()/2),20);
        
        JButton valoriButton = new JButton("Introducere valori");
        JButton sortareButton = new JButton("Sortare valori");
        
        GridLayout grid = new GridLayout(1,2,5,5);
        JPanel p = new JPanel();
        p.setLayout(grid);
        p.add(valoriButton);
        p.add(sortareButton);
        
        Fereastra f = new Fereastra();
        frame.setVisible(true);
        
        JTextField t = new JTextField("", 100);
        
        valoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                boolean ok = true;
                while (ok) {
                    try {
                        a.setX(frame.getWidth()/2);
                        a.setY(20);
                        
                        String s = JOptionPane.showInputDialog(frame, "Introduceti valoarea unui nod.",
                                "Introducere valori de sortat", JOptionPane.INFORMATION_MESSAGE);
                        
                        if (s != null) {
                            a.insereazaNod(Integer.parseInt(s));
                            x = a.getX();
                            y = a.getY();
                            prevX = a.getPrevX();
                            prevY = a.getPrevY();
                            val = a.getValoare() + "";
                        }
                        ok = false;
                    } catch (NumberFormatException exc) {
                        JOptionPane.showMessageDialog(frame, "Introduceti numai valori intregi",
                                "Atentie", JOptionPane.WARNING_MESSAGE);
                        System.out.println(exc);
                    }
                }
                f.repaint();
                
            }
        });
        
        sortareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                a.inordine(a.getRadacina());
                t.setText("Valorile sortate sunt urmatoarele: " +
                        a.getSirValori().substring(0, a.getSirValori().length()-2));
            }
            
        });
        
        BorderLayout border = new BorderLayout();
        frame.setLayout(border);
        frame.add(p,BorderLayout.NORTH);
        frame.add(f,BorderLayout.CENTER);
        frame.add(t,BorderLayout.SOUTH);
        
        frame.setVisible(true);
    }
    
    public static JFrame getFrame() {
        return frame;
    }
    
    private class Fereastra extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            if (x != 0) {
                g.drawOval(x-10, y-10, 20, 20);
                if (val.length() == 1) {
                    g.drawString(val, x-3, y+5);
                } else {
                    g.drawString(val, x-6, y+5);
                }
                
                if (prevY != 0) {
                    g.drawLine(prevX, prevY+10, x, y-10);
                }
            }
        }
    }
}
